import { Controller, All, Req, Res, Get } from '@nestjs/common';
import { AppService, microservices } from './app.service';
import { Request, Response } from 'express';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  @Get('/microservices')
  getAllMicroservices() {
    return microservices;
  }

  @All('/**')
  async getHello(@Req() req: Request, @Res() res: Response) {
    const [isError, response] = await this.appService.makeSwitchRequest(
      req.url,
      req.method,
      req.body,
      req.headers,
    );
    if (isError) {
      res.status(response.statusCode);
    }
    return res.json(response);
  }
}
