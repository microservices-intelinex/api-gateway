import {
  BadRequestException,
  Injectable,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
export const microservices = {
  books: process.env.MICROSERVICE_BOOKS_URL,
  ratings: process.env.MICROSERVICE_RATINGS_URL,
  users: process.env.MICROSERVICE_USERS,
  notifications: process.env.MICROSERVICE_NOTIFICATIONS,
};
@Injectable()
export class AppService {
  constructor(private readonly httpService: HttpService) {}
  async makeSwitchRequest(
    path: string,
    method: string,
    data: object,
    headers: any,
  ): Promise<any[]> {
    const makeRequest = async (
      host: string,
      pathIn: string,
      data: any,
      headers: any,
      method: string,
    ) => {
      return new Promise((resolve, reject) => {
        const objectRequest = {
          url: host + pathIn,
          method: method,
        };
        if (!!Object.values(data).length) {
          objectRequest['data'] = data;
        }
        if (headers['authorization']) {
          objectRequest['headers'] = {
            authorization: headers['authorization'],
          };
        }
        this.httpService.request(objectRequest).subscribe({
          next(e) {
            resolve(e.data);
          },
          error(e) {
            reject(e?.response?.data ?? e);
          },
        });
      });
    };

    try {
      if (path.includes('books')) {
        await makeRequest(
          microservices.users + '/auth/access',
          '',
          {},
          headers,
          'POST',
        );
        return [
          false,
          await makeRequest(microservices.books, path, data, headers, method),
        ];
      }
      if (path.includes('ratings')) {
        await makeRequest(
          microservices.users + '/auth/access',
          '',
          {},
          headers,
          'POST',
        );
        return [
          false,
          await makeRequest(microservices.ratings, path, data, headers, method),
        ];
      }
      if (path.includes('users') || path.includes('auth')) {
        return [
          false,
          await makeRequest(microservices.users, path, data, headers, method),
        ];
      }
      if (path.includes('notitications')) {
        return [
          false,
          await makeRequest(
            microservices.notifications,
            path,
            data,
            headers,
            method,
          ),
        ];
      }

      throw new BadRequestException(
        "The microservice that you are looking wasn't found",
      );
    } catch (err) {
      if (err?.code?.includes('ECONNREFUSED')) {
        throw new BadRequestException(
          'Network error, please register the microservice for: ' + path,
        );
      }
      Logger.error(err);
      if (!err?.statusCode) {
        throw new BadRequestException('Microservice error: ' + path);
      }
      return [true, err];
    }
  }
}
